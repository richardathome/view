<?php

namespace Richbuilds\View\Tests;

use PHPUnit\Framework\TestCase;
use Richbuilds\View\View;

class ViewTest extends TestCase
{
    private View $view;

    protected function setUp(): void
    {
        $this->view = new View(__DIR__ . '/templates/');
    }

    public function testRenderWithNoVariables(): void
    {
        $result = $this->view->render('hello_world');
        $expected = $this->getExpectedResult('hello_world.xml');
        $this->assertXmlStringEqualsXmlString($expected, $result);
    }

    public function testRenderWithVariables(): void
    {
        $result = $this->view->render('hello_name', ['name' => 'Alice']);
        $expected = $this->getExpectedResult('hello_name.xml');
        $this->assertXmlStringEqualsXmlString($expected, $result);
    }

    private function getExpectedResult(string $filename): string
    {
        $file = __DIR__ . '/expected_results/' . $filename;
        return file_get_contents($file);
    }
}