# View

## A Simple Template Engine

### Example Useage:

```php
$view = new View('template_base_directory');

$result = $this->view->render('template', ['foo'=>$foo, 'bar'=>2]);
```