<?php

namespace Richbuilds\View;

class View
{
    private string $templateRoot;

    public function __construct(string $templateRoot)
    {
        if (!file_exists($templateRoot) || !is_readable($templateRoot)) {
            throw new \InvalidArgumentException(sprintf('Template root "%s" does not exist or is not readable', $templateRoot));
        }

        $this->templateRoot = rtrim($templateRoot, '/') . '/';
    }

    public function render(string $template, array $variables = []): string
    {
        $file = $this->templateRoot . $template . '.tpl.php';

        if (!file_exists($file) || !is_readable($file)) {
            throw new \InvalidArgumentException(sprintf('Template file "%s" does not exist or is not readable', $file));
        }

        extract($variables, EXTR_SKIP);

        ob_start();
        include $file;
        $content = ob_get_clean();

        return $content;
    }
}